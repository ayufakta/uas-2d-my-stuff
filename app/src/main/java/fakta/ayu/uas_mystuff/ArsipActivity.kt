package fakta.ayu.uas_mystuff

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_arsip.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONException

class ArsipActivity : AppCompatActivity() {

    companion object{
        val urlunars = "http://192.168.43.239/noteuas/query_unarsp.php"
    }

    lateinit var arsipAdapter: ArsipAdapter
    var daftarArsip = mutableListOf<HashMap<String, String>>()
    val urlshowarsip = "http://192.168.43.239/noteuas/show_data_arsp.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arsip)

        arsipAdapter = ArsipAdapter(this,daftarArsip)
        listArsip.layoutManager = LinearLayoutManager(this)
        listArsip.adapter = arsipAdapter

        showDataArsip()
    }

    fun showDataArsip() {
        val request = object : StringRequest(
            Request.Method.POST, urlshowarsip,
            Response.Listener { response ->
                try {
                    daftarArsip.clear()
                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length() - 1)) {
                        val jsonObject = jsonArray.getJSONObject(x)
                        var arsip = HashMap<String, String>()
                        arsip.put("id", jsonObject.getString("id"))
                        arsip.put("judul", jsonObject.getString("judul"))
                        arsip.put("isi", jsonObject.getString("isi"))
                        arsip.put("tanggal", jsonObject.getString("tanggal"))
                        arsip.put("kategori", jsonObject.getString("kategori"))
                        arsip.put("url", jsonObject.getString("url"))
                        daftarArsip.add(arsip)
                    }
                    arsipAdapter.notifyDataSetChanged()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
