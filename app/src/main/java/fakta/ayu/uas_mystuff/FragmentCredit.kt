package fakta.ayu.uas_mystuff

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class FragmentCredit :Fragment(){

    lateinit var thisParent : AboutActivity
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as AboutActivity
        return inflater.inflate(R.layout.frag_credit, container, false)
    }
}