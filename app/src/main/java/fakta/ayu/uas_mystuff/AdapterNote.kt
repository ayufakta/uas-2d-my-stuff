package fakta.ayu.uas_mystuff

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import fakta.ayu.uas_mystuff.ArsipActivity.Companion.urlunars
import fakta.ayu.uas_mystuff.KategoriActivity.Companion.urldeletektg
import fakta.ayu.uas_mystuff.KategoriActivity.Companion.urlupdatektg
import fakta.ayu.uas_mystuff.MainNoteActivity.Companion.urlarsip
import fakta.ayu.uas_mystuff.MainNoteActivity.Companion.urldelete
import kotlinx.android.synthetic.main.row_ktg_update.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import org.json.JSONException

class AdapterNote (context: Context, val dataNote : MutableList<HashMap<String, String>>) :


    RecyclerView.Adapter<AdapterNote.HolderDataNote>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataNote {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_note,p0, false)
        return HolderDataNote(v)
    }

    override fun getItemCount(): Int {
        return dataNote.size
    }


    override fun onBindViewHolder(p0: HolderDataNote, p1: Int) {
        val data = dataNote.get(p1)
        p0.txIdNote.setText(data.get("id"))
        p0.txJudul.setText(data.get("judul"))
        p0.txIsi.setText(data.get("isi"))
        p0.txTanggal.setText(data.get("tanggal"))
        p0.txKtg.setText(data.get("kategori"))

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)

        p0.btnArsip.setOnClickListener{
            AlertDialog.Builder(nCtx)
                .setTitle("Warning")
                .setMessage("Apakah kamu yakin ingin mengarsipkan note?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener{dialog, which ->
                    val request = object : StringRequest(Method.POST, urlarsip,
                        Response.Listener { response ->
                            try {
                                JSONObject(response)
                                Toast.makeText(nCtx, "Sukses Diarsipkan", Toast.LENGTH_LONG).show()

                            } catch (e: JSONException) {
                                Toast.makeText(nCtx, "Gagal", Toast.LENGTH_LONG).show()
                            }
                            dataNote.removeAt(p1)
                            notifyItemRemoved(p1)
                            notifyItemRangeChanged(p1, dataNote.size)
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(nCtx, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
                        }){
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String,String>()
                            hm.put("mode","arsip")
                            hm.put("id",p0.txIdNote.text.toString())
                            return hm
                        }
                    }
                    val queue = Volley.newRequestQueue(nCtx)
                    queue.add(request)
                })
                .setNegativeButton("No", { dialog, which ->  })
                .setIcon(R.drawable.ic_warning_black_24dp)
                .show()
        }

        p0.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataId", data.get("id"))
            bundle.putString("dataJudul", data.get("judul"))
            bundle.putString("dataIsi", data.get("isi"))
            bundle.putString("dataTanggal", data.get("tanggal"))
            bundle.putString("dataUrl", data.get("url"))

            val i = Intent(nCtx, EditNoteActivity::class.java)
            i.putExtras(bundle)
            startActivity(nCtx, i, bundle)
        }

        p0.btnDelete.setOnClickListener {
            AlertDialog.Builder(nCtx)
                .setTitle("Warning")
                .setMessage("Apakah kamu yakin ingin menghapus?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener{dialog, which ->
                    val request = object : StringRequest(Method.POST, urldelete,
                        Response.Listener { response ->
                            try {
                                JSONObject(response)
                                Toast.makeText(nCtx, "Sukses Terhapus", Toast.LENGTH_LONG).show()

                            } catch (e: JSONException) {
                                Toast.makeText(nCtx, "Gagal", Toast.LENGTH_LONG).show()
                            }
                            dataNote.removeAt(p1)
                            notifyItemRemoved(p1)
                            notifyItemRangeChanged(p1, dataNote.size)
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(nCtx, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
                        }){
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String,String>()
                            val nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                            hm.put("mode","delete")
                            hm.put("id",p0.txIdNote.text.toString())
                            return hm
                        }
                    }
                    val queue = Volley.newRequestQueue(nCtx)
                    queue.add(request)
                })
                .setNegativeButton("No", { dialog, which ->  })
                .setIcon(R.drawable.ic_warning_black_24dp)
                .show()
        }
    }


    val nCtx = context

    class HolderDataNote(v: View): RecyclerView.ViewHolder(v){
        val txIdNote = v.findViewById<TextView>(R.id.txIdNote)
        val txJudul = v.findViewById<TextView>(R.id.txJudul)
        val txIsi = v.findViewById<TextView>(R.id.txIsi)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)
        val txKtg = v.findViewById<TextView>(R.id.txKtg)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val btnDelete = v.findViewById<ImageButton>(R.id.btnDelete)
        val btnArsip = v.findViewById<ImageButton>(R.id.btnArsip)
    }
}

class KategoriAdapter (context: Context, val dataKategori : MutableList<HashMap<String, String>>) :
    RecyclerView.Adapter<KategoriAdapter.HolderDataKategori>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataKategori {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kategori,p0, false)
        return HolderDataKategori(v)
    }

    override fun getItemCount(): Int {
        return dataKategori.size
    }

    override fun onBindViewHolder(p0: HolderDataKategori, p1: Int) {
        val data = dataKategori.get(p1)
        p0.txIdKategori.setText(data.get("id"))
        p0.txKategori.setText(data.get("kategori"))
        p0.btnDelete.setOnClickListener{
            AlertDialog.Builder(nCtx)
                .setTitle("Warning")
                .setMessage("Apakah kamu yakin ingin menghapus?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener{dialog, which ->
                    val request = object : StringRequest(Method.POST, urldeletektg,
                        Response.Listener { response ->
                            try {
                                JSONObject(response)
                                Toast.makeText(nCtx, "Sukses Terhapus", Toast.LENGTH_LONG).show()

                            } catch (e: JSONException) {
                                Toast.makeText(nCtx, "Gagal", Toast.LENGTH_LONG).show()
                            }
                            dataKategori.removeAt(p1)
                            notifyItemRemoved(p1)
                            notifyItemRangeChanged(p1, dataKategori.size)
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(nCtx, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
                        }){
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String,String>()
                            hm.put("mode","delete")
                            hm.put("id",p0.txIdKategori.text.toString())
                            return hm
                        }
                    }
                    val queue = Volley.newRequestQueue(nCtx)
                    queue.add(request)
                })
                .setNegativeButton("No", { dialog, which ->  })
                .setIcon(R.drawable.ic_warning_black_24dp)
                .show()
        }
        p0.itemView.setOnClickListener{
            val inflater = LayoutInflater.from(nCtx)
            val view = inflater.inflate(R.layout.row_ktg_update, null)

            val data = dataKategori.get(p1)
            view.editText.setText(data.get("kategori"))

            AlertDialog.Builder(nCtx)
                .setTitle("Edit Kategori")
                .setView(view)
                .setPositiveButton("Yes", DialogInterface.OnClickListener{dialog, which ->
                    val request = object : StringRequest(Method.POST, urlupdatektg,
                        Response.Listener { response ->
                            try {
                                JSONObject(response)
                                Toast.makeText(nCtx, "Sukses Melakukan Update", Toast.LENGTH_LONG).show()
                                dataKategori[p1]["kategori"] = view.editText.text.toString()
                                notifyDataSetChanged()
                            } catch (e: JSONException) {
                                Toast.makeText(nCtx, "Gagal", Toast.LENGTH_LONG).show()
                            }
                            notifyDataSetChanged()
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(nCtx, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
                        }){
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String,String>()
                            hm.put("mode","update")
                            hm.put("id", p0.txIdKategori.text.toString())
                            hm.put("kategori", view.editText.text.toString())
                            return hm
                        }
                    }
                    val queue = Volley.newRequestQueue(nCtx)
                    queue.add(request)
                })
                .setNegativeButton("No", { dialog, which ->  })
                .show()
        }
    }
    val nCtx = context
    class HolderDataKategori(v: View): RecyclerView.ViewHolder(v){
        val txIdKategori = v.findViewById<TextView>(R.id.tvIdKategori)
        val txKategori = v.findViewById<TextView>(R.id.tvRvKtg)
        val btnDelete = v.findViewById<ImageButton>(R.id.btnDelKategori)
    }
}

class ArsipAdapter (context: Context, val dataArsip : MutableList<HashMap<String, String>>) :
    RecyclerView.Adapter<ArsipAdapter.HolderDataArsip>() {
    override fun onCreateViewHolder(
        p0: ViewGroup, p1: Int
    ): ArsipAdapter.HolderDataArsip {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_arsip,p0, false)
        return HolderDataArsip(v)
    }

    override fun getItemCount(): Int {
        return dataArsip.size
    }

    override fun onBindViewHolder(p0: HolderDataArsip, p1: Int) {
        val data = dataArsip.get(p1)
        p0.txIdNote.setText(data.get("id"))
        p0.txJudul.setText(data.get("judul"))
        p0.txIsi.setText(data.get("isi"))
        p0.txTanggal.setText(data.get("tanggal"))
        p0.txKtg.setText(data.get("kategori"))

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)

        p0.btnUnArs.setOnClickListener {
            AlertDialog.Builder(nCtx)
                .setTitle("Warning")
                .setMessage("Apakah kamu yakin ingin menghapus note dari arsip?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener{dialog, which ->
                    val request = object : StringRequest(Method.POST, urlunars,
                        Response.Listener { response ->
                            try {
                                JSONObject(response)
                                Toast.makeText(nCtx, "Sukses", Toast.LENGTH_LONG).show()

                            } catch (e: JSONException) {
                                Toast.makeText(nCtx, "Gagal", Toast.LENGTH_LONG).show()
                            }
                            dataArsip.removeAt(p1)
                            notifyItemRemoved(p1)
                            notifyItemRangeChanged(p1, dataArsip.size)
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(nCtx, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
                        }){
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String,String>()
                            val nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                            hm.put("id",p0.txIdNote.text.toString())
                            return hm
                        }
                    }
                    val queue = Volley.newRequestQueue(nCtx)
                    queue.add(request)
                })
                .setNegativeButton("No", { dialog, which ->  })
                .setIcon(R.drawable.ic_warning_black_24dp)
                .show()
        }
    }

    val nCtx = context
    class HolderDataArsip(v: View): RecyclerView.ViewHolder(v){
        val txIdNote = v.findViewById<TextView>(R.id.txIdNoteArs)
        val txJudul = v.findViewById<TextView>(R.id.txJudulArs)
        val txIsi = v.findViewById<TextView>(R.id.txIsiArs)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggalArs)
        val txKtg = v.findViewById<TextView>(R.id.txKtgArs)
        val photo = v.findViewById<ImageView>(R.id.imageViewArs)
        val btnUnArs = v.findViewById<ImageButton>(R.id.btnUnArs)
    }

}