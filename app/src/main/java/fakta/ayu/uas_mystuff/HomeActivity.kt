package fakta.ayu.uas_mystuff

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "Setting"
    val RC_SUKSES: Int = 100

    //    variabel ganti warna
    var bg: String = ""
    var fontheader: String = ""
    val DEF_BACK = bg
    val DEF_BACK_FONT_HEAD = fontheader
    val FIELD_BACK = "BACKGROUND"
    val FIELD_FONTHEAD = "BACKGROUND_FONTHEADER"

    //    var ganti ukuran font
    var fSubTitle: Int = 30
    var fDetail: Int = 30
    val DEF_FONT_DETAIL = fDetail
    val FIELD_FONT_DETAIL = "Font_Size_Detail"

    //    var edt text
    var detail: String = "CHOOSE ! ! !"
    val DEF_DETAIL = detail
    val FIELD_TEXT = "text"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txtDetail.setText(preferences.getString(FIELD_TEXT, DEF_DETAIL))
        fDetail = preferences.getInt(FIELD_FONT_DETAIL, DEF_FONT_DETAIL)
        bg = preferences.getString(FIELD_BACK, DEF_BACK).toString()
        fontheader = preferences.getString(FIELD_FONTHEAD, DEF_BACK_FONT_HEAD).toString()

        background()
        fontheadercolor()
        detail()

        btnNote.setOnClickListener(this)

        btnGoogle.setOnClickListener {
            var webUri = "https://google.com"
            var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))

            startActivity(intentInternet)
        }
        btnImages.setOnClickListener {
            var intentGallery = Intent()
            intentGallery.setType("image/*")
            intentGallery.setAction(Intent.ACTION_GET_CONTENT)

            startActivity(Intent.createChooser(intentGallery, "Pilih Dokumen..."))
        }
        btnCalcu.setOnClickListener {
            var intent = Intent(this,CalcuActivity::class.java)
            startActivity(intent)
        }
        btnGame.setOnClickListener {
            var intent = Intent(this,GameActivity::class.java)
            startActivity(intent)
        }
        btnQr.setOnClickListener {
            var intent = Intent(this,QrCodeActivity::class.java)
            startActivity(intent)
        }
        btnMaps.setOnClickListener {
            val i = Intent(this, MapsActivity::class.java)
            startActivity(i)
        }
        btnPlay.setOnClickListener {
            val i = Intent(this, PlayActivity::class.java)
            startActivity(i)
        }
        btnCamera.setOnClickListener {
            val i = Intent(this, GaleriActivity::class.java)
            startActivity(i)
        }
    }


    fun background() {
        if (bg == "BLUE") {
            constraintlayout.setBackgroundColor(Color.BLUE)
        } else if (bg == "YELLOW") {
            constraintlayout.setBackgroundColor(Color.YELLOW)
        } else if (bg == "GREEN") {
            constraintlayout.setBackgroundColor(Color.GREEN)
        } else if (bg == "BLACK") {
            constraintlayout.setBackgroundColor(Color.BLACK)
        }
    }

    fun fontheadercolor() {
        if (fontheader == "Blue") {
            txtHeader.setTextColor(Color.BLUE)
        } else if (fontheader == "Yellow") {
            txtHeader.setTextColor(Color.YELLOW)
        } else if (fontheader == "Green") {
            txtHeader.setTextColor(Color.GREEN)
        } else if (fontheader == "Black") {
            txtHeader.setTextColor(Color.BLACK)
        } else {
            txtHeader.setTextColor(Color.WHITE)
        }
    }


    fun detail() {
        txtDetail.textSize = fDetail.toFloat()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_SUKSES)
                txtDetail.setText(data?.extras?.getString("isidetail"))
            bg = data?.extras?.getString("bgColor").toString()
            fontheader = data?.extras?.getString("fhColor").toString()
            fSubTitle = data?.extras?.getInt("tHead").toString().toInt()
            fDetail = data?.extras?.getInt("tTitle").toString().toInt()
            preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val prefEdit = preferences.edit()
            prefEdit.putString(FIELD_BACK,bg)
            prefEdit.putString(FIELD_FONTHEAD, fontheader)
            prefEdit.putInt(FIELD_FONT_DETAIL, fDetail)
            prefEdit.putString(FIELD_TEXT,detail)
            prefEdit.commit()

            background()
            fontheadercolor()
            detail()


        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnNote -> {
                var intent = Intent(this,MainNoteActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnInflater = menuInflater

        mnInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.itmAbout -> {
                val i = Intent(this, AboutActivity::class.java)
                startActivity(i)
            }
            R.id.itmSetting -> {
                var intent = Intent(this, SettingActivity::class.java)
                intent.putExtra("detail", txtDetail.text.toString())
                startActivityForResult(intent, RC_SUKSES)
                return true
            }
            R.id.itmContact -> {
                val i = Intent(this, SosmedActivity::class.java)
                startActivity(i)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
