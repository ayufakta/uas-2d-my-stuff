package fakta.ayu.uas_mystuff

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_note.*
import kotlinx.android.synthetic.main.row_note_update.*
import org.json.JSONArray
import org.json.JSONException

class MainNoteActivity : AppCompatActivity() {
    companion object{
        val urldelete = "http://192.168.43.239/noteuas/query_del.php"
        val urlupdate = "http://192.168.43.239/noteuas/query_upd.php"
        val urlinsert = "http://192.168.43.239/noteuas/query_ins.php"
        val urlarsip = "http://192.168.43.239/noteuas/query_ars.php"
        lateinit var mediaHelper: MediaHelper
    }
    lateinit var mediaHelper: MediaHelper
    lateinit var noteAdapter: AdapterNote
    lateinit var prodiAdapter: ArrayAdapter<String>
    var daftarNote = mutableListOf<HashMap<String, String>>()
    var daftarProdi = mutableListOf<String>()
    val url = "http://192.168.43.239/noteuas/show_data.php"
    val urlshowprodi = "http://192.168.43.239/noteuas/get_nama_prodi.php"
    val urldelete = "http://192.168.43.239/noteuas/query_del.php"
    val urlupdate = "http://192.168.43.239/noteuas/query_upd.php"
    val urlinsert = "http://192.168.43.239/noteuas/query_ins.php"
    var imStr = ""
    var pilihProdi = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        noteAdapter = AdapterNote(this,daftarNote)
        mediaHelper = MediaHelper(this)
        listNote.layoutManager = LinearLayoutManager(this)
        listNote.adapter = noteAdapter

        fabAdd.setOnClickListener{
            val i = Intent(this, AddNoteActivity::class.java)
            startActivity(i)
        }

        editSearch.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }


            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val judulNote = editSearch.text.toString()
                showDataNote(judulNote)
            }
        })


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnInflater = menuInflater

        mnInflater.inflate(R.menu.main_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.itmAbout -> {
                val i = Intent(this, AboutActivity::class.java)
                startActivity(i)
            }
            R.id.itmArsip -> {
                val i = Intent(this, ArsipActivity::class.java)
                startActivity(i)
            }
            R.id.itmKategori -> {
                val i = Intent(this, KategoriActivity::class.java)
                startActivity(i)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        showDataNote("")
    }

    fun showDataNote(judulNote : String) {
        val request = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener { response ->
                try {
                    daftarNote.clear()
                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length() - 1)) {
                        val jsonObject = jsonArray.getJSONObject(x)
                        var note = HashMap<String, String>()
                        note.put("id", jsonObject.getString("id"))
                        note.put("judul", jsonObject.getString("judul"))
                        note.put("isi", jsonObject.getString("isi"))
                        note.put("tanggal", jsonObject.getString("tanggal"))
                        note.put("kategori", jsonObject.getString("kategori"))
                        note.put("url", jsonObject.getString("url"))
                        daftarNote.add(note)
                    }
                    noteAdapter.notifyDataSetChanged()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("judul",judulNote)
                return hm
            }
            }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data, imageView)
            }
        }
    }
}
