package fakta.ayu.uas_mystuff

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kategori.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_ktg_update.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class KategoriActivity : AppCompatActivity() {

    lateinit var KategoriAdapter: KategoriAdapter
    var daftarKategori = mutableListOf<HashMap<String, String>>()

    companion object{
        val urlshow = "http://192.168.43.239/noteuas/get_kategori.php"
        val urldeletektg = "http://192.168.43.239/noteuas/query_del_ktg.php"
        val urlupdatektg = "http://192.168.43.239/noteuas/query_upd_ktg.php"
        val urlinsertktg = "http://192.168.43.239/noteuas/query_ins_ktg.php"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kategori)

        KategoriAdapter = KategoriAdapter(this,daftarKategori)
        listKategori.layoutManager = LinearLayoutManager(this)
        listKategori.adapter = KategoriAdapter

        showKategori()

        fabAddKategori.setOnClickListener{
            val inflater = LayoutInflater.from(this)
            val view = inflater.inflate(R.layout.row_ktg_update, null)

            AlertDialog.Builder(this)
                .setTitle("Tambah Kategori")
                .setView(view)
                .setPositiveButton("Yes", DialogInterface.OnClickListener{ dialog, which ->
                    val request = object : StringRequest(Method.POST, urlinsertktg,
                        Response.Listener { response ->
                            try {
                                JSONObject(response)
                                Toast.makeText(this, "Sukses Menambahkan Kategori", Toast.LENGTH_LONG).show()
                                showKategori()
                            } catch (e: JSONException) {
                                Toast.makeText(this, "Gagal", Toast.LENGTH_LONG).show()
                            }
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(this, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
                        }){
                        override fun getParams(): MutableMap<String, String> {
                            val hm = HashMap<String,String>()
                            hm.put("mode","insert")
                            hm.put("kategori", view.editText.text.toString())
                            return hm
                        }
                    }
                    val queue = Volley.newRequestQueue(this)
                    queue.add(request)
                })
                .setNegativeButton("No", { dialog, which ->  })
                .show()
        }
        }

    fun showKategori(){
        val request = object : StringRequest(
            Request.Method.POST, urlshow,
            Response.Listener { response ->
                try {
                    daftarKategori.clear()
                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length() - 1)) {
                        val jsonObject = jsonArray.getJSONObject(x)
                        var kategori = HashMap<String, String>()
                        kategori.put("id", jsonObject.getString("id"))
                        kategori.put("kategori", jsonObject.getString("kategori"))
                        daftarKategori.add(kategori)
                    }
                    KategoriAdapter.notifyDataSetChanged()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){}
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}
