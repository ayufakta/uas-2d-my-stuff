package fakta.ayu.uas_mystuff

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragAuthor : FragmentAuthor
    lateinit var fragCredit : FragmentCredit
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragAuthor = FragmentAuthor()
        fragCredit = FragmentCredit()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itmAuth -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragAuthor).commit()
                frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itmCredit -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragCredit).commit()
                frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itmApp -> frameLayout.visibility = View.GONE
        }

        return true
        }
    }