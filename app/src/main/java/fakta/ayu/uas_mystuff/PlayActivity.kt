package fakta.ayu.uas_mystuff

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.MediaController
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_play.*
import java.util.concurrent.TimeUnit

class PlayActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    val daftarLagu = intArrayOf(R.raw.music_1, R.raw.music_2, R.raw.music_3)
    val daftarVideo = intArrayOf(R.raw.vid1, R.raw.vid2, R.raw.vid3)
    val daftarCover = intArrayOf(R.drawable.cover_1, R.drawable.cover_2, R.drawable.cover_3)
    val daftarJudul= arrayOf("WHen She Loved Me - Sarah McLachlan", "Wild World - Mr. Big", "Grow old WithMe - Tom Odell")

    var posLaguSkrg = 0
    var posVidSkrg = 0
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer.create(this, daftarLagu[posLaguSkrg])
        seekSong.max=100
        seekSong.progress=0
        seekSong.setOnSeekBarChangeListener(this)
        btnPlay.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        mediaController.setPrevNextListeners(nextVid, prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVidSkrg)
    }

    var nextVid = View.OnClickListener { v:View ->
        if(posVidSkrg<(daftarVideo.size-1)) posVidSkrg++
        else posVidSkrg = 0
        videoSet(posVidSkrg)
    }

    var prevVid = View.OnClickListener { v:View ->
        if(posVidSkrg>0) posVidSkrg--
        else posVidSkrg = daftarVideo.size-1
        videoSet(posVidSkrg)
    }

    fun videoSet( pos : Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        val menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(pos : Int){
        mediaPlayer = MediaPlayer.create(this, daftarLagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(daftarCover[pos])
        txJudulLagu.setText(daftarJudul[pos])
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
        if(posLaguSkrg<(daftarLagu.size-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
        if(posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = daftarLagu.size-1
        }
        audioPlay(posLaguSkrg)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress=currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this, 50)

        }
    }
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it)  }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay -> {
                audioPlay(posLaguSkrg)
            }
            R.id.btnNext -> {
                audioNext()
            }
            R.id.btnPrev -> {
                audioPrev()
            }
            R.id.btnStop -> {
                audioStop()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnInflater = menuInflater

        mnInflater.inflate(R.menu.menu_option, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.itmAbout -> {
                val i = Intent(this, AboutActivity::class.java)
                startActivity(i)
            }
            R.id.itmContact -> {
                val i = Intent(this, SosmedActivity::class.java)
                startActivity(i)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
