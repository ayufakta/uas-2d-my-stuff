package fakta.ayu.uas_mystuff

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_add_note.*
import kotlinx.android.synthetic.main.row_note.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class AddNoteActivity : AppCompatActivity(), View.OnClickListener {

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0

    lateinit var mediaHelper: MediaHelper
    lateinit var noteAdapter: AdapterNote
    lateinit var prodiAdapter: ArrayAdapter<String>
    var daftarNote = mutableListOf<HashMap<String, String>>()
    var daftarProdi = mutableListOf<String>()
    val url = "http://192.168.43.239/noteuas/show_data.php"
    val urlgetkategori = "http://192.168.43.239/noteuas/get_kategori.php"
    val urldelete = "http://192.168.43.239/noteuas/query_del.php"
    val urlupdate = "http://192.168.43.239/noteuas/query_upd.php"
    val urlinsert = "http://192.168.43.239/noteuas/query_ins.php"
    var imStr = ""
    var pilihProdi = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnInflater = menuInflater

        mnInflater.inflate(R.menu.menu_top_update,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.itmClose -> {
                finish()
            }
            R.id.itmSave -> {
                var alertDialog = AlertDialog.Builder(this)
                    .setTitle("Warning")
                    .setMessage("Apakah yakin ingin menambahkan note?")
                    .setPositiveButton("Yes", DialogInterface.OnClickListener{ dialog, which ->
                        queryInsertUpdateDelete("insert")
                    })
                    .setNegativeButton("No", DialogInterface.OnClickListener{ dialog, which ->  })
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)

        prodiAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarProdi)
        spinProdi.adapter = prodiAdapter
        spinProdi.onItemSelectedListener = itemSelected
        mediaHelper = MediaHelper(this)

        imUpload.setOnClickListener(this)

        val cal: Calendar = Calendar.getInstance()

        bulan = cal.get(Calendar.MONTH)
        tahun = cal.get(Calendar.YEAR)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        jam = cal.get(Calendar.HOUR_OF_DAY)
        menit = cal.get(Calendar.MINUTE)

        editTanggal.text = "$hari - ${bulan+1} - $tahun | $jam:$menit"

        btnDatePicker.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in Toast
                Toast.makeText(this, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                editTanggal.text = "$dayOfMonth - ${monthOfYear + 1} - $year | $jam:$menit"
                tahun = year
                bulan = monthOfYear
                hari = dayOfMonth

            }, tahun, bulan, hari)
            dpd.show()
        }

        btnTimePicker.setOnClickListener {
            val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                // Display Selected date in Toast
                Toast.makeText(this, """$hourOfDay:$minute""", Toast.LENGTH_LONG).show()
                editTanggal.text = "$hari - ${bulan+1} - $tahun | $hourOfDay:$minute"

            }, jam, menit, true)
            tpd.show()
        }

    }

    override fun onStart() {
        super.onStart()
        getNamaProdi()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinProdi.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data, imUpload)
            }
        }
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST,urlgetkategori,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("kategori"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,urlinsert,
            Response.Listener { response ->
                try {
                    val jsonObject = JSONObject(response)
                    val error = jsonObject.getString("kode")
                    if(error.equals("000")){
                        Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                        imUpload.setImageResource(R.drawable.ic_add_a_photo_black_24dp)
                        editNoteJudul.text.clear()
                        editNoteIsi.text.clear()
                    }
                    else{
                        Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                    }
                }catch (e: JSONException){
                    Toast.makeText(this, "Gagal", Toast.LENGTH_LONG).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".png"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("judul",editNoteJudul.text.toString())
                        hm.put("isi",editNoteIsi.text.toString())
                        hm.put("tanggal",editTanggal.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("kategori",pilihProdi)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
