package fakta.ayu.uas_mystuff

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_note.*
import kotlinx.android.synthetic.main.activity_edit_note.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class EditNoteActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mediaHelper: MediaHelper
    val urlupdate = "http://192.168.43.239/noteuas/query_upd.php"
    val urlgetkategori = "http://192.168.43.239/noteuas/get_kategori.php"
    var imStr = ""
    var pilihProdi = ""

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imOnUp-> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent, mediaHelper.getRcGallery())
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnInflater = menuInflater

        mnInflater.inflate(R.menu.menu_top_update,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.itmClose -> {
                finish()
            }
            R.id.itmSave -> {
                var alertDialog = AlertDialog.Builder(this)
                    .setTitle("Warning")
                    .setMessage("Apakah yakin ingin melakukan update?")
                    .setPositiveButton("Yes", DialogInterface.OnClickListener{ dialog, which ->
                        queryInsertUpdateDelete("update")
                    })
                    .setNegativeButton("No", DialogInterface.OnClickListener{ dialog, which ->  })
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    lateinit var prodiAdapter : ArrayAdapter<String>
    var daftarProdi = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)

        mediaHelper = MediaHelper(this)

        prodiAdapter = ArrayAdapter(this,R.layout.custom_spinner,daftarProdi)
        spinUp.adapter = prodiAdapter
        spinUp.onItemSelectedListener = itemSelected

        imOnUp.setOnClickListener(this)

        getNamaProdi()

        val tvData1 = findViewById<TextView>(R.id.tvId)
        val tvData2 = findViewById<TextView>(R.id.edJudulUpdate)
        val tvData3 = findViewById<TextView>(R.id.edIsiOnUpdate)
        val tvData4 = findViewById<TextView>(R.id.tvTanggalOnUp)
        val imgView = findViewById<ImageView>(R.id.imOnUp)

        val bundle = intent.extras
        tvData1.setText(bundle?.getString("dataId"))
        tvData2.setText(bundle?.getString("dataJudul"))
        tvData3.setText(bundle?.getString("dataIsi"))
        tvData4.setText(bundle?.getString("dataTanggal"))
        if (!bundle?.getString("dataUrl").equals(""))
            Picasso.get().load(bundle?.getString("dataUrl")).into(imgView)
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinUp.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data, imOnUp)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,urlupdate,
            Response.Listener { response ->
                    val jsonObject = JSONObject(response)
                    val error = jsonObject.getString("kode")
                    if(error.equals("000")){
                        Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    }
                    else{
                        Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                    }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                    Date()
                )+".jpg"
                when(mode){
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id", tvId.text.toString())
                        hm.put("judul", edJudulUpdate.text.toString())
                        hm.put("isi", edIsiOnUpdate.text.toString())
                        hm.put("tanggal", tvTanggalOnUp.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("kategori", pilihProdi)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST,urlgetkategori,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("kategori"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
