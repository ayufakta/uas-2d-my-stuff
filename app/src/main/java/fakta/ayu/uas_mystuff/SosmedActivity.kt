package fakta.ayu.uas_mystuff


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_sosmed.*
import kotlinx.android.synthetic.main.activity_sosmed.view.*
import kotlinx.android.synthetic.main.activity_sosmed.view.admin1
import kotlinx.android.synthetic.main.activity_sosmed.view.admin2


class SosmedActivity : AppCompatActivity(), View.OnClickListener  {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.admin1-> {
                var webUri = "mailto:mystuffluv@gmail.com"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)
            }
            R.id.admin2 -> {
                var webUri = "mailto:faktaaaa@gmail.com"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sosmed)
        admin1.setOnClickListener(this)
        admin2.setOnClickListener(this)
    }

}
