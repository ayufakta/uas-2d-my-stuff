package fakta.ayu.uas_mystuff

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_galeri.*
import org.json.JSONObject

class GaleriActivity: AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_camera ->{
                requestPermission()
            }
            R.id.btnKirim ->{
                uploadFile()
            }
        }
    }

    lateinit var photoHelper: MediaHelper
    val url = "http://192.168.43.239/noteuas/upload_photo.php"
    var imstr = ""
    var namaFile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galeri)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e:Exception){
            e.printStackTrace()
        }

        photoHelper = MediaHelper(this)
        btn_camera.setOnClickListener(this)
        btnKirim.setOnClickListener(this)
//        //BUTTON CLICK
//        btn_camera.setOnClickListener{
//            var i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//
//            startActivityForResult(i, 123)
//        }
//
        btn_galeri.setOnClickListener {
            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
        }
    }

    fun uploadFile(){
        val request = object : StringRequest(Method.POST,url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val kode = jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this, "Upload Sukses", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this, "Upload Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("imstr",imstr)
                hm.put("namaFile", namaFile)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermission() = runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.CAMERA){
        fileUri = photoHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent, photoHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == photoHelper.getRcCamera()){
                imstr = photoHelper.getBitmapToString(fileUri, image_view)
                namaFile = photoHelper.getMyFileName()
            }
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            image_view.setImageURI(data?.data)
        }

        if (requestCode == 123){
            var bmp = data?.extras?.get("data") as? Bitmap
            image_view.setImageBitmap(bmp)
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    companion object {
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Tidak Diijinkan", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
